﻿using COMP3973_Lab3.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COMP3973_Lab3.Data
{
    public class DummyData
    {
        public static void Initialize(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                context.Database.EnsureCreated();

                // Not seeded
                if (context.Provinces == null && !context.Provinces.Any())
                {
                    var provinces = DummyData.GetProvinces().ToArray();
                    context.Provinces.AddRange(provinces);
                    context.SaveChanges();

                    var cities = DummyData.GetCities(context).ToArray();
                    context.Cities.AddRange(cities);
                    context.SaveChanges();
                }
            }
        }

        public static List<Province> GetProvinces()
        {
            List<Province> provinces = new List<Province>()
            {
                new Province()
                {
                    ProvinceCode = "BC",
                    ProvinceName = "British Columbia",
                },

                new Province()
                {
                    ProvinceCode = "AB",
                    ProvinceName = "Alberta",
                },

                new Province()
                {
                    ProvinceCode = "ON",
                    ProvinceName = "Ontario",
                },
            };

            return provinces;
        }

        public static List<City> GetCities(ApplicationDbContext context)
        {
            List<City> cities = new List<City>()
            {
                new City()
                {
                    CityName = "Vancouver",
                    Population = 4000000,
                    ProvinceCode = context.Provinces.Find("BC").ProvinceCode,
                },

                new City()
                {
                    CityName = "Richmond",
                    Population = 2000000,
                    ProvinceCode = context.Provinces.Find("BC").ProvinceCode,
                },

                new City
                {
                    CityName = "Burnaby",
                    Population = 3000000,
                    ProvinceCode = context.Provinces.Find("BC").ProvinceCode,
                },

                new City
                {
                    CityName = "Calgary",
                    Population = 2000000,
                    ProvinceCode = context.Provinces.Find("AB").ProvinceCode,
                },

                new City
                {
                    CityName = "Edmonton",
                    Population = 2000000,
                    ProvinceCode = context.Provinces.Find("AB").ProvinceCode,
                },

                new City
                {
                    CityName = "Wainwright",
                    Population = 100000,
                    ProvinceCode = context.Provinces.Find("AB").ProvinceCode,
                },

                new City
                {
                    CityName = "Ottawa",
                    Population = 4000000,
                    ProvinceCode = context.Provinces.Find("ON").ProvinceCode,
                },

                new City
                {
                    CityName = "Toronto",
                    Population = 5000000,
                    ProvinceCode = context.Provinces.Find("ON").ProvinceCode,
                },

                new City
                {
                    CityName = "Markham",
                    Population = 600000,
                    ProvinceCode = context.Provinces.Find("ON").ProvinceCode,
                },
            };

            return cities;
        }
    }
}
